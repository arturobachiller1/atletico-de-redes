@extends('layouts.app')

@section('content')
<div class="container mt-5">
    
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 1</div>
                            <div class="panel-body"> </div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 2</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 3</div>
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 4</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 5</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 6</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 7</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 8</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 9</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 10</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 11</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 12</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 13</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 14</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 15</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-dark text-white">FOTO 16</div>
                            <div class="panel-body"></div>
                        </div>
                    </div>
            

            <h1>Lista de estudios
                <div class="float-right">                
                    <a href="/studies/create" class="btn btn-primary">
                    Nuevo                
                    </a>
                    <a href="/equipo" class="btn btn-primary">
                    Nuevo (sesion)
                    </a>    
                </div>
            </h1>
    
    
        <table class="table table-striped">
        <tr>
            <th>Goles</th>
            <th>Goles en contra</th>
            <th>Partidos jugados</th>
            <th>Partidos ganados</th>
            <th>Partidos empatados</th>
            <th>Partidos perdidos</th>
            <th>Puesto en la liga</th>
        </tr>
        
        @forelse ($equipos as $equipo)
        <tr>
            <td>{{$equipo->goles}} </td>
            <td>{{$equipo->name}} </td>
            <td>{{$equipo->abreviation}} </td>
            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}">Ver</a></td>
            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}/edit">Editar</a></td>
            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}/edit">Editar</a></td>
        </tr>
        @empty
        <tr>
            <td colspan="3">No hay estudios registrados</td>
        </tr>
        @endforelse
        </table>


        



        </div>


</div>
@endsection


