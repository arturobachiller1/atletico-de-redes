@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Actualización de una noticia</h1>

            <form action="/noticias/{{$noticia->id}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div>
                    <label for="titulo">Titulo</label>
                    <input type="text" name="titulo" value="{{$noticia->titulo}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div>
                    <label for="subtitulo">Subtitulo</label>
                    <input type="text" name="subtitulo" value="{{$noticia->subtitulo}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="descripcion">Descripcion</label>
                    <input type="text" name="descripcion" value="{{$noticia->descripcion}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="imagen">Imagen</label>
                    <input type="text" name="imagen" value="{{$noticia->imagen}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div>
                    <input type="submit" value="actualizar">
                </div>
            </form>

            @if(count($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
            @endif
        </div>
    </div>



</div>
@endsection