@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8  mt-5">
        <h1>Creación de productos</h1>

        <form action="/noticias" method="post">
        @csrf
        <div>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" value="{{ old('titulo') }}"> 
            @error('titulo')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="subtitulo">Subtitulo</label>
            <input type="text" name="subtitulo" value="{{ old('subtitulo') }}"> 
            @error('subtitulo')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="descripcion">Descripcion</label>
            <input type="text" name="descripcion" value="{{ old('descripcion') }}"> 
            @error('descripcion')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="imagen">Imagen</label>
            <input type="text" name="imagen" value="{{ old('imagen') }}"> 
            @error('imagen')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <input type="submit" value="crear"> 
        </div>        
        </form>



        @if(count($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach            
        </div>
        @endif
    </div>
    </div>

</div>
@endsection