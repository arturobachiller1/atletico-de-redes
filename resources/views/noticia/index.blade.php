@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-10 mt-5">

            <div class="row col-lg-12 col-md-12 col-sm-12 ">
                @forelse($noticias as $noticia)

                <div class="noticia col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <a class="text-black" href="/noticias/{{$noticia->id}}">
                        <img class="imgnoticia mt-4" src="{{'img/' . $noticia->imagen}}" alt="">
                        <h4 class="titulonoticia mt-2">
                            <?php
                            echo "" . $noticia->titulo . "";
                            ?>
                        </h4>
                        <h6 class="titulonoticia text-secondary mt-4">
                            <?php
                            echo "" . $noticia->subtitulo . "";
                            ?>
                        </h6>
                    </a>
                </div>

                @empty
                <td colspan="3">No hay noticias registradas</td>

                @endforelse


                <!-- En este caso si que funciona sin array porque definimos arriba noticia-->
                @can ('view', $noticia)
                <h1>Lista de noticias
                    <div class="float-right">
                        <a href="/noticias/create" class="btn btn-primary">
                            Nuevo
                        </a>
                    </div>
                </h1>
                <table class="table table-striped">
                    <tr>
                        <th>Titular</th>
                        <th>Subtitulo</th>
                        <th>Descripcion</th>
                        <th>imagen</th>
                    </tr>
                    @forelse ($noticias as $noticia)
                    <tr>
                        <td>{{$noticia->titulo}} </td>
                        <td>{{$noticia->subtitulo}} </td>
                        <td>{{$noticia->descripcion}} </td>
                        <td>{{$noticia->imagen}} </td>

                        <td> <a class="btn btn-primary btn-sm" href="/noticias/{{$noticia->id}}">Ver</a></td>
                        <td> <a class="btn btn-primary btn-sm" href="/noticias/{{$noticia->id}}/edit">Editar</a></td>
                        <td>
                            <form action="/noticias/{{$noticia->id}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input class="btn btn-danger btn-sm" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No hay estudios registrados</td>
                    </tr>
                    @endforelse
                </table>
                @endcan

            </div>
        </div>
    </div>
    @endsection