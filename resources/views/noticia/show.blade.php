@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
           
            <div class="row">
                <div class="col-10">
                    <img src="<?= asset('img/' . "$noticia->imagen")?> " >
                    <h4 class="mt-5"><strong>{{$noticia->titulo}}</strong></h4>
                    <h6> <strong>{{ $noticia->subtitulo }}</strong></h6>
                    <p class="mt-5">{{ $noticia->descripcion }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection