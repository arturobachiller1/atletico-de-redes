@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8  mt-5">
            <div class="card mt-5">
                <div class="card-header">{{ __('Atletico de redes') }} </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Ey ' . Auth::user()->name . " te hemos estado esperando!!") }} 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
