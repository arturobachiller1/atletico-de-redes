@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Actualización de un perfil de jugador</h1>



            <form action="/jugadores/{{$jugador->id}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" value="{{$jugador->nombre}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="apellido">Apellido</label>
                    <input type="text" name="apellido" value="{{$jugador->apellido}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="dorsal">Dorsal</label>
                    <input type="text" name="dorsal" value="{{$jugador->dorsal}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="posicion">Posicion</label>
                    <input type="text" name="posicion" value="{{$jugador->posicion}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="nacionalidad">Nacionalidad</label>
                    <input type="text" name="nacionalidad" value="{{$jugador->nacionalidad}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="altura">Altura</label>
                    <input type="text" name="altura" value="{{$jugador->altura}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div>
                    <label for="peso">Peso</label>
                    <input type="text" name="peso" value="{{$jugador->peso}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div>
                    <label for="fechanacimiento">Fecha de nacimiento</label>
                    <input type="text" name="fechanacimiento" value="{{$jugador->fechanacimiento}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <label for="frase">Frase</label>
                    <input type="text" name="frase" value="{{$jugador->frase}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <label for="imagen">imagen</label>
                    <input type="text" name="imagen" value="{{$jugador->imagen}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div>
                    <input type="submit" value="actualizar">
                </div>
            </form>

            @if(count($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
            @endif
        </div>
    </div>



</div>
@endsection