@extends('layouts.app')

@section('content')

<div class="container margin-topo">

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-5">







        <!--bucle en el que definimos como se van a ver los divs de jugadores utiliza bootstrap 3 por los paneles-->
        @forelse($jugadores as $jugador)
        <div class="panel-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a class="text-black" href="/jugadores/{{$jugador->id}}">
                <div class="panel panel-default text-center">
                    <div class="panel-body ">
                        <h1 class="h1-dorsal">
                            <?php
                            echo "" . $jugador->dorsal . "";
                            ?>
                        </h1>
                        <h4 class='h4-nombre'>
                            <?php
                            echo "" . $jugador->nombre . "";
                            ?>
                        </h4>
                        <img class="insertarimg img-fluid" src="{{'img/' . $jugador->imagen}}" alt="">
                    </div>
                </div>
            </a>
        </div>
        @empty

        <td colspan="3">No hay jugadores registrados en el club</td>

        @endforelse



        <!-- En este caso si que funciona sin array porque definimos arriba noticia-->
        @can ('view', $jugador)
        <h1>Jugadores del equipo
            <div class="float-right">
                <a href="/jugadores/create" class="btn btn-primary">
                    Nuevo Fichaje
                </a>
            </div>
        </h1>


        <table class="table table-striped">
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dorsal</th>
                <th>posicion</th>
                <th>nacionalidad</th>
                <th>altura</th>
                <th>peso</th>
                <th>fecha-nacimiento</th>
            </tr>


            @forelse ($jugadores as $jugador)
            <tr>
                <td>{{$jugador->id}} </td>
                <td>{{$jugador->nombre}} </td>
                <td>{{$jugador->apellido}} </td>
                <td>{{$jugador->dorsal}} </td>
                <td>{{$jugador->posicion}} </td>
                <td>{{$jugador->nacionalidad}} </td>
                <td>{{$jugador->altura}} </td>
                <td>{{$jugador->peso}} </td>
                <td>{{$jugador->fechanacimiento}} </td>
                <td> <a class="btn btn-primary btn-sm" href="/jugadores/{{$jugador->id}}">Ver</a></td>
                <td> <a class="btn btn-primary btn-sm" href="/jugadores/{{$jugador->id}}/edit">Editar</a></td>
                <td>
                    <form action="/jugadores/{{$jugador->id}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input class="btn btn-danger btn-sm" type="submit" value="Borrar">
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="3">No hay estudios registrados</td>
            </tr>
            @endforelse
        </table>

        @endcan




    </div>


</div>
@endsection