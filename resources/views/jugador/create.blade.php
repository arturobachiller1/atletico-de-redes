@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h1>Creación de jugadores</h1>

        <form action="/jugadores" method="post">
        @csrf
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="{{ old('nombre') }}"> 
            @error('nombre')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="apellido">Apellido</label>
            <input type="text" name="apellido" value="{{ old('apellido') }}"> 
            @error('apellido')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="dorsal">Dorsal</label>
            <input type="text" name="dorsal" value="{{ old('dorsal') }}"> 
            @error('dorsal')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>


        <div>
            <label for="posicion">Posicion</label>
            <input type="text" name="posicion" value="{{ old('posicion') }}"> 
            @error('posicion')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="nacionalidad">Nacionalidad</label>
            <input type="text" name="nacionalidad" value="{{ old('nacionalidad') }}"> 
            @error('nacionalidad')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="altura">Altura</label>
            <input type="text" name="altura" value="{{ old('altura') }}"> 
            @error('altura')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="peso">Peso</label>
            <input type="text" name="peso" value="{{ old('peso') }}"> 
            @error('peso')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="fechanacimiento">Fecha de nacimiento</label>
            <input type="text" name="fechanacimiento" value="{{ old('fechanacimiento') }}"> 
            @error('fechanacimiento')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="frase">Frase del jugador</label>
            <input type="text" name="frase" value="{{ old('frase') }}"> 
            @error('frase')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="imagen">Imagen</label>
            <input type="text" name="imagen" value="{{ old('imagen') }}"> 
            @error('imagen')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <input type="submit" value="crear"> 
        </div>        
        </form>



        @if(count($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach            
        </div>
        @endif
    </div>
    </div>

</div>
@endsection