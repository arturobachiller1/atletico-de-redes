@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            <div class="fondo row">

                
                <h1 class="col-lg-4 col-md-4 col-sm-6 col-12 d-sm-inline">{{$jugador->nombre}}<br>
                    {{$jugador->apellido}}
                </h1>
                <h3 id="show-frase" class="col-lg-4 col-md-4 col-sm-6 col-12 ">{{$jugador->frase}}</h3>



                @switch($jugador->nacionalidad)
                @case($jugador->nacionalidad == "España")
                <div class="col-lg-4 col-md-4 col-sm-4 col-8  mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/espana.jpg')   ?>" alt=""></div>
                </div>
                @break

                @case($jugador->nacionalidad == "Chile")
                <div class="col-lg-3 col-md-4 col-sm-4 col-8 mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/chile.jpg')   ?>" alt=""></div>
                </div>
                @break

                @case($jugador->nacionalidad == "Peru")
                <div class="col-lg-3 col-md-4 col-sm-4 col-8 mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/peru.jpg')   ?>" alt=""></div>
                </div>
                @break

                @case($jugador->nacionalidad == "Jamaica")
                <div class="col-lg-3 col-md-4 col-sm-4 col-8 mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/jamaica.jpg')   ?>" alt=""></div>
                </div>
                @break

                @case($jugador->nacionalidad == "Rumania")
                <div class="col-lg-3 col-md-4 col-sm-4 col-8 mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/rumania.jpg')   ?>" alt=""></div>
                </div>
                @break

                @case($jugador->nacionalidad == "Belgica")
                <div class="col-lg-3 col-md-4 col-sm-4 col-8 mb-5">
                    <h3 class="show-secundario ">Nacionalidad</h3>
                    <div class="show-secundario-2"> {{ $jugador->nacionalidad }} <img class="imagen-bandera" src="<?= asset('img/belgica.jpg')   ?>" alt=""></div>
                </div>
                @break
                @endswitch

            </div>


            <div class="row mt-5">


                <div class="col-lg-3 col-md-3 col-6 mt-">
                    <h3 class="">Altura</h3>
                    <div class=""> {{ $jugador->altura }}</div>

                    <h3 class=" ">Peso</h3>
                    <div class=""> {{ $jugador->peso }} </div>
                </div>
                <img class="img-fluid col-lg-3 col-md-4 col-4" src="<?= asset('img/notfound.png') ?>"  alt="">
                <div class="col-lg-2 col-md-1 d-lg-inline">
                </div>
                @switch($jugador->posicion)
                @case($jugador->posicion == "Portero")
                <div class="col-lg-4 col-md-4 col-sm-4 col-2">
                    <h3 class=""> Demarcacion</h3>
                    <img class="demarcacion" src="<?= asset('img/descarga por.png') ?>" alt=""><br>
                    <h4>{{$jugador->posicion}}</h4>
                </div>
                @break

                @case($jugador->posicion == "Defensa")
                <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                    <h3 class=""> Demarcacion</h3>
                    <img class="demarcacion" src="<?= asset('img/descarga dfc.png') ?>" alt=""><br>
                    <h4>{{$jugador->posicion}}</h4>
                </div>
                @break

                @case($jugador->posicion == "Mediocentro")
                <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                    <h3 class=""> Demarcacion</h3>
                    <img class="demarcacion" src="<?= asset('img/descarga mc.png') ?>" alt=""><br>
                    <h4>{{$jugador->posicion}}</h4>
                </div>
                @break
                @case($jugador->posicion == "Delantero")
                <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                    <h3 class=""> Demarcacion</h3>
                    <img class="demarcacion" src="<?= asset('img/descarga dc.png') ?>" alt=""><br>
                    <h4>{{$jugador->posicion}}</h4>
                </div>
                @break

                @endswitch

                
            </div>
        </div>
    </div>
</div>
@endsection