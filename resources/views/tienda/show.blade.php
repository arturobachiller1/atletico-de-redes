@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div class="row">
                <!--La pagina esta formada por dos partes la izquierda que muestra la foto del producto y la derecha con el metodo de pago-->
                <div class="col-6">
                    <img class="img-fluid" src="<?= asset('img/' . "$tienda->imagen") ?> " width="300" height="300">
                    <h4>{{$tienda->nombre}}</h4>
                    <p><strong>Descripción:</strong> {{ $tienda->descripcion }}</p>
    
                    <p><strong>Precio: </strong>{{ $tienda->precio }}€</p>

                </div>
                <div class="col-xl-6 col-lg-6 col-12">
                    <img class="img-fluid mt-2 mb-4" src="<?= asset('img/bizum.jpg') ?>" width="300" height="100">

                    <!--Ojo en el show de tienda esta el update de clientes se pasa el id del producto y el precio-->

                    <form action="/clientes/{{$tienda->id}} javascript:grecaptcha.reset();" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="idproducto" value="<?= $tienda->id ?>">
                        <input type="hidden" name="precio" value="<?= $tienda->precio ?>">

                        <div>
                            <label for="ciudad">Ciudad</label>
                            <input type="text" name="ciudad" value="{{ old('ciudad') }}">
                            @error('ciudad')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="direccion">Direccion</label>
                            <input type="text" name="direccion" value="{{ old('direccion') }}">
                            @error('direccion')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="codigo_postal">Codigo post.</label>
                            <input type="text" name="codigo_postal" value="{{ old('codigo_postal') }}">
                            @error('codigo_postal')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="telefono">Telefono</label>
                            <input type="text" name="telefono" value="{{ old('telefono') }}">
                            @error('telefono')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <label for="cbox2"><input type="checkbox" name="validation" value="1"> He leido y acepto <a class="text-danger" href="/condiciones">los terminos y condiciones</a></label>
                        @error('validation')
                        <div class="alert alert-danger"> Tienes que marcar la casilla de he leido y acepto los terminos y condiciones</div>
                        @enderror
                        <p>Toda la informacion obtenida en este formulario no sera utilizada para enviar correos spam ni notificaciones.</p>
                        <div>
                            {!! htmlFormSnippet() !!}
                            @error('g-recaptcha-response')
                            <div class="alert alert-danger"><strong>{{ $errors->first('g-recaptcha-response') }}</strong></div>
                            @enderror
                        </div>
                        
                        <input type="submit" value="crear">


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection