@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h1>Creación de productos</h1>

        <form action="/tienda" method="post">
        @csrf
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="{{ old('nombre') }}"> 
            @error('nombre')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="tipo">Tipo</label>
            <input type="text" name="tipo" value="{{ old('tipo') }}"> 
            @error('tipo')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="precio">Precio</label>
            <input type="text" name="precio" value="{{ old('precio') }}"> 
            @error('precio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="imagen">Imagen</label>
            <input type="text" name="imagen" value="{{ old('imagen') }}"> 
            @error('imagen')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <label for="descripcion">Descripcion</label>
            <input type="text" name="descripcion" value="{{ old('descripcion') }}"> 
            @error('descripcion')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div>
            <input type="submit" value="crear"> 
        </div>        
        </form>



        @if(count($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach            
        </div>
        @endif
    </div>
    </div>

</div>
@endsection