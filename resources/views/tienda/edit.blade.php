@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Actualización de un producto</h1>



            <form action="/tienda/{{$tienda->id}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" value="{{$tienda->nombre}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div>
                    <label for="tipo">Tipo</label>
                    <input type="text" name="tipo" value="{{$tienda->tipo}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="precio">Precio</label>
                    <input type="text" name="precio" value="{{$tienda->precio}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="imagen">Imagen</label>
                    <input type="text" name="imagen" value="{{$tienda->imagen}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>



                <div>
                    <label for="descripcion">Descripcion</label>
                    <input type="text" name="descripcion" value="{{$tienda->descripcion}}">
                    @error('abreviation')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>




                <div>
                    <input type="submit" value="actualizar">
                </div>
            </form>

            @if(count($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
            @endif
        </div>
    </div>



</div>
@endsection