@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">

            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @forelse($tiendas as $producto)

                <div class="panel-group col-lg-4 col-md-6 col-sm-6 col-12">

                    <div class="panel panel-default">
                        <a class="text-black" href="/tienda/{{$producto->id}}">
                            <div class="panel-bodyt">
                                <h4 class='h4-nombre mg-extra-lf ml-3'>
                                    <?php
                                    echo "" . $producto->nombre . "";
                                    ?>
                                </h4>

                                <img class="insertarimgt ml-5 md-ml-3 ml-lg-3" src="{{'img/' . $producto->imagen}}" alt="">
                                <h5 class='h4-nombre'>
                                    Precio:
                                    <?php
                                    echo "" . $producto->precio . " €";
                                    ?>
                                </h5>
                            </div>
                        </a>

                    </div>

                </div>
                @empty

                <td colspan="3">No hay estudios registrados</td>

                @endforelse


                <!-- No se puede visualizar si es un array y el unico que lo puede ver es el admin role = 3-->
                @can ('ViewAny', App\Tienda::class)
                <h1>Lista de articulos
                    <div class="float-right">
                        <a href="/tienda/create" class="btn btn-primary">
                            Nuevo
                        </a>
                    </div>
                </h1>
                <table class="table table-striped">
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Precio</th>
                    </tr>
                    @forelse ($tiendas as $tienda)
                    <tr>
                        <td>{{$tienda->nombre}} </td>
                        <td>{{$tienda->tipo}} </td>
                        <td>{{$tienda->precio}} </td>
                        <td> <a class="btn btn-primary btn-sm" href="/tienda/{{$tienda->id}}">Ver</a></td>
                        <td> <a class="btn btn-primary btn-sm" href="/tienda/{{$tienda->id}}/edit">Editar</a></td>
                        <td>
                            <form action="/tienda/{{$tienda->id}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input class="btn btn-danger btn-sm" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No hay estudios registrados</td>
                    </tr>
                    @endforelse
                </table>
            @endcan

            </div>
        </div>
    </div>
    @endsection