@extends('layouts.app')

@section('content')

<div class="container-fluid mt-5">


    <!-- jugadores usados 0, 8, 9 articulos usados 0, 1, 2 -->
    <div class="row mt-5">
        <div class="col-xl-1 col-lg-1 d-md-none col-sm-1 col-xs-1 d-lg-inline">
            <!-- Esto es solamente un separador -->
        </div>

        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-xs-1 mt-5">
            <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
                <h4>Jugadores destacados de la temporada</h4>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a class="text-black" href="/jugadores/{{$jugadores[0]->id}}">
                            <div class="panel panel-default text-center">
                                <div class="panel-body">
                                    <h1 class="h1-dorsal ml-5">
                                        <?php
                                        echo "" . $jugadores[0]->dorsal . "";
                                        ?>
                                    </h1>
                                    <h4 class='h4-nombre ml-5'>
                                        <?php
                                        echo "" . $jugadores[0]->nombre . "";
                                        ?>
                                    </h4>
                                    <img class="insertarimg img-fluid" src="{{'img/' . $jugadores[0]->imagen}}" alt="">
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="carousel-item">
                        <a class="text-black" href="/jugadores/{{$jugadores[9]->id}}">
                            <div class="panel panel-default text-center">
                                <div class="panel-body ">
                                    <h1 class="h1-dorsal ml-5">
                                        <?php
                                        echo "" . $jugadores[9]->dorsal . "";
                                        ?>
                                    </h1>
                                    <h4 class='h4-nombre ml-5'>
                                        <?php
                                        echo "" . $jugadores[9]->nombre . "";
                                        ?>
                                    </h4>
                                    <img class="insertarimg img-fluid" src="{{'img/' . $jugadores[9]->imagen}}" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a class="text-black" href="/jugadores/{{$jugadores[8]->id}}">
                            <div class="panel panel-default text-center">
                                <div class="panel-body ">
                                    <h1 class="h1-dorsal ml-5">
                                        <?php
                                        echo "" . $jugadores[8]->dorsal . "";
                                        ?>
                                    </h1>
                                    <h4 class='h4-nombre ml-5'>
                                        <?php
                                        echo "" . $jugadores[8]->nombre . "";
                                        ?>
                                    </h4>
                                    <img class="insertarimg img-fluid" src="{{'img/' . $jugadores[7]->imagen}}" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <a class="carousel-control-prev ml-1" href="#carouselExampleControls" role="button" data-slide="prev">
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                </a>
            </div>
        </div>
        <div class="col-xl-1 col-lg-1 d-md-none col-sm-1 col-xs-1 d-lg-inline">
            <!-- Esto es solamente un separador -->
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-1 col-xs-1 mt-5">
            <h4>Noticias</h4>
            <div class="noticia mt-5">
                <a class="text-black" href="/noticias/{{$noticias[0]->id}}">
                    <img class="imgnoticia mt-4" src="{{'img/' . $noticias[0]->imagen}}" alt="">
                    <h4 class="titulonoticia mt-2">
                        <?php
                        echo "" . $noticias[0]->titulo . "";
                        ?>
                    </h4>
                    <h6 class="titulonoticia text-secondary mt-4">
                        <?php
                        echo "" . $noticias[0]->subtitulo . "";
                        ?>
                    </h6>
                </a>
            </div>
        </div>

        <div class="col-xl-1 col-lg-1 col-md-2 d-sm-none col-xs-1 d-md-inline">
            <!-- Esto es solamente un separador -->
        </div>

        <div id="carouselExampleControls2" class="carousel slide carousel-fade col-xl-3 col-lg-4 col-md-4  col-xs-3 mt-5 d-sm-none d-md-inline" data-ride="carousel">
            <h4>Productos destacados de la tienda</h4>
            <div class="carousel-inner">
                <div class="carousel-item ">
                    <div class="panel panel-default">
                        <a class="text-black" href="/tienda/{{$tienda[0]->id}}">
                            <div class="panel-bodyt">
                                <h4 class='h4-nombre mg-extra-lf ml-1'>
                                    <?php
                                    echo "" . $tienda[0]->nombre . "";
                                    ?>
                                </h4>
                                <img class="insertarimgtt" src="{{'img/' . $tienda[0]->imagen}}" width="100" height="100" alt="">
                                <h5 class='h4-nombre mt-4'>
                                    Precio:
                                    <?php
                                    echo "" . $tienda[0]->precio . "";
                                    ?>
                                </h5>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="carousel-item active">
                    <div class="panel panel-default">
                        <a class="text-black" href="/tienda/{{$tienda[1]->id}}">
                            <div class="panel-bodyt">
                                <h4 class='h4-nombre mg-extra-lf ml-1'>
                                    <?php
                                    echo "" . $tienda[1]->nombre . "";
                                    ?>
                                </h4>

                                <img class="insertarimgtt" src="{{'img/' . $tienda[1]->imagen}}" width="100" height="100" alt="">
                                <h5 class='h4-nombre mt-4'>
                                    Precio:
                                    <?php
                                    echo "" . $tienda[1]->precio . "";
                                    ?>
                                </h5>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="panel panel-default">
                        <a class="text-black" href="/tienda/{{$tienda[2]->id}}">
                            <div class="panel-bodyt">
                                <h4 class='h4-nombre mg-extra-lf ml-1'>
                                    <?php
                                    echo "" . $tienda[2]->nombre . "";
                                    ?>
                                </h4>

                                <img class="insertarimgtt" src="{{'img/' . $tienda[2]->imagen}}" width="100" height="100" alt="">
                                <h5 class='h4-nombre mt-4'>
                                    Precio:
                                    <?php
                                    echo "" . $tienda[2]->precio . "";
                                    ?>
                                </h5>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
            </a>
            <a class="carousel-control-next " href="#carouselExampleControls2" role="button" data-slide="next">
            </a>
        </div>


        <!--<div class="fotofondo"></div>
        <div class="fotofondo2"></div>-->



        <div class="col-xl-1 col-lg-1 d-md-none d-sm-none col-xs-1">
            <!-- Esto es solamente un separador -->
        </div>
    </div>

    <div class="row mt-5">
        <h1 class="mt-5 ml-5"> Datos del equipo en la temporada 2021-2022</h1>
        <div class="mt-5 col-xl-6 col-lg-6 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Goles a favor esta temporada: {{ $equipos[0]->goles}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-6 col-lg-6 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Goles en contra esta temporada: {{ $equipos[0]->golesencontra}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Partidos jugados: {{ $equipos[0]->partidosj}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Partidos ganados: {{ $equipos[0]->partidosg}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-3- col-lg-3 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Partidos empatados: {{ $equipos[0]->partidose}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Partidos perdidos: {{ $equipos[0]->partidosp}}</h4>
            </div>
        </div>
        <div class="mt-5 col-xl-4 col-lg-4 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Puesto en la liga: {{ $equipos[0]->puesto}}</h4>
            </div>
        </div>

        <div class="mt-5 col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-1">
            <div class="">
                <h4>Estadio: {{ $equipos[0]->estadio}}</h4>
                <img class="img-fluid" src="{{'img/morecambe.jpg'}}" alt="" width="200px" height="150px">
            </div>
        </div>
    </div>


    <!-- No se puede visualizar si es un array y el unico que lo puede ver es el admin role = 3-->
    @can ('viewAny', App\Equipo::class)
    <h1>Lista de estudios
        <div class="float-right">
            <a href="/equipos/create" class="btn btn-primary">
                Nuevo
            </a>
        </div>
    </h1>


    <table class="table table-striped">
        <tr>
            <th>Goles</th>
            <th>Goles en contra</th>
            <th>Partidos jugados</th>
            <th>Partidos ganados</th>
            <th>Partidos empatados</th>
            <th>Partidos perdidos</th>
            <th>Puesto en la liga</th>
            <th>Estadio</th>
        </tr>



        @forelse ($equipos as $equipo)
        <tr>
            <td>{{$equipo->goles}} </td>
            <td>{{$equipo->golesencontra}} </td>
            <td>{{$equipo->partidosj}} </td>
            <td>{{$equipo->partidosg}} </td>
            <td>{{$equipo->partidose}} </td>
            <td>{{$equipo->partidosp}} </td>
            <td>{{$equipo->puesto}} </td>
            <td>{{$equipo->estadio}} </td>


            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}">Ver</a></td>
            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}/edit">Editar</a></td>
            <td> <a class="btn btn-primary btn-sm" href="/studies/{{$equipo->id}}/edit">Editar</a></td>
        </tr>
        @empty
        <tr>
            <td colspan="3">No hay estudios registrados</td>
        </tr>

    </table>
    @endforelse
    @endcan





</div>


</div>
@endsection