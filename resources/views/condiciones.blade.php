@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8  mt-5">
            <div class="card mt-5">
                <div class="card-header">{{ __('Politicas y condiciones de la compra de un articulo') }} </div>

                <div class="card-body">
                <p>Bizum es un proveedor de servicios de pago de España, fruto de la colaboración de la gran mayoría de las entidades bancarias del país para crear un sistema de pagos instantáneos entre particulares y de compras en comercios. El sistema de pago de bizum consiste en recoger el numero de telefono y enviar un mensaje en el que muestra el precio, despues el usuario debe aceptar el pago. <div class="text-danger"> El nombre el cual va a enviar el bizum es Arturo B.R, el resto cuidado porque podria ser un intento de estafa.</div> Toda la informacion obtenida en este formulario no sera utilizada para enviar correos spam ni notificaciones.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
