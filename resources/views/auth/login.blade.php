@extends('layouts.app')

@section('content')
<!--el header tiene la imagen de fondo-->
<header class="header">
    <div class="container-fluid mt-6">
        <div class="row justify-content-center custom-margin">
            <div class="col-sm-6 col-md-4">

                        <form method="POST" class="shadow-lg p-4 text-white" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">

                                <div class="col-md-6 offset-md-3">
                                <i class="fas fa-user"></i> <label for="email" class="pl-2 font-weight-bold">Email</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                                    <small class="form-text">Nunca compartiremos tu correo con nadie</small>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-6 offset-md-3">
                                    <i class="fas fa-key"></i><label for="pass" class="pl-2 font-weight-bold">Contraseña</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Recuerdame') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-outline-success shadow-sm font-weight-bold">
                                        {{ __('Iniciar sesion') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Has olvidado tu contraseña?') }}
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Add bg-primary in form tag if want form background color-->
    <!--text-white if want text color white-->


    
</header>



@endsection