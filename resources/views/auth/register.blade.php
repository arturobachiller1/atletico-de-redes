@extends('layouts.app')

@section('content')
<!--el header tiene la imagen de fondo-->
<header class="header">
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <form method="POST" class="shadow-lg p-4 text-white" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group row">
                       <label for="name" class="col-md-4 col-form-label text-md-right">  <i class="mr-3 far fa-address-card"></i>{{ __('Nombre') }}</label>

                        <div class="col-md-6">
                            <input id="name" ptype="text" placeholder="Nombre" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right"> <i class="mr-3 fas fa-user"></i>{{ __('Introduce el email') }}</label>

                        <div class="col-md-6">
                            <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">  <i class="mr-3 fas fa-key"></i>{{ __('Cotranseña') }}</label>

                        <div class="col-md-6">
                           <input id="password" placeholder="Contraseña" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right"> <i class="mr-3 fas fa-key"></i>{{ __('Confirma Contraseña') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" placeholder="Contraseña" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                    <!--Este es el google recaptcha-->
                    <div class="col-md-6 offset-md-4 mb-3">
                        {!! htmlFormSnippet() !!}
                        @error('g-recaptcha-response')
                        <div class="alert alert-danger"><strong>{{ $errors->first('g-recaptcha-response') }}</strong></div>
                        @enderror
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-outline-success mt-3  shadow-sm font-weight-bold">
                                {{ __('Registrar') }}
                            </button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
</header class="header">
@endsection





