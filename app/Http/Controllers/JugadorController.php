<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use Illuminate\Http\Request;

class JugadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jugadores = Jugador::all(); //toma todos los registros
        return view('jugador.index', ['jugadores' => $jugadores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jugador.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required',
            'apellido' => 'required',
            'dorsal' => 'required|unique:jugadors,dorsal,$id|integer|max:99',
            'posicion' => 'required',
            'nacionalidad' => 'required',
            'altura' => 'required',
            'peso' => 'required',
            'fechanacimiento' => 'required|date_format:Y-m-d'
        ];
        //    protected $fillable = ['id', 'nombre', 'apellido', 'dorsal', 'posicion', 'nacionalidad', 'altura', 'peso',
        // 'fecha-nacimiento ' => 'datetime:Y-m-d', 'frase', 'imagen'];

        $request->validate($rules);
        //version corta
        Jugador::create($request->all());

        //version larga, comentada
        // $study = new Study;
        // $study->code = $request->code;
        // $study->name = $request->name;
        // $study->abreviation = $request->abreviation;
        // $study->save();
        // header('Location .....');
        return redirect('/jugadores');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $jugador = Jugador::find($id);
        return view('jugador.show', ['jugador' => $jugador]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jugador = Jugador::find($id);
        return view('jugador.edit', ['jugador' => $jugador]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombre' => 'required',
            'apellido' => 'required',
            'dorsal' => 'required|integer|max:99',
            'posicion' => 'required',
            'nacionalidad' => 'required',
            'altura' => 'required',
            'peso' => 'required',
            'fechanacimiento' => 'required|date_format:Y-m-d'
        ];
        $request->validate($rules);

        //version larga, comentada
        // $study->code = $request->code;
        // $study->name = $request->name;
        // $study->abreviation = $request->abreviation;
        //version corta
        $jugador = Jugador::find($id);
        $jugador->fill($request->all());
        $jugador->save();
        return redirect('/jugadores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jugador = Jugador::find($id);
        $jugador->delete();
        return back();
    }
}
