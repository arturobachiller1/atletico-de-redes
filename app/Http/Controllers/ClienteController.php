<?php

namespace App\Http\Controllers;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*puede haber un fallo y es que el id que estas pasando es el de tienda y aqui deberia utilizarse el de cliente una opcion es probar el metodo post
        es decir hacerlo en el store pasando la id por ahi*/
        $rules = [
            'idproducto' => 'required',
            'precio' => 'required',
            'ciudad' => 'required',
            'direccion' => 'required',
            'codigo_postal' => 'required|numeric|digits:5',
            'telefono' => 'required|numeric|digits:9',
            'validation' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
        
         // $study->name = $request->name;
         $request->validate($rules);
         if (Auth::check()) {
            $cliente = new Cliente;
            $cliente->idproducto = $request->idproducto;
            $preciorebaja = $request->precio / 10;
            $cliente->precio = $request->precio - $preciorebaja;
            $cliente->ciudad = $request->ciudad;
            $cliente->direccion = $request->direccion;
            $cliente->codigo_postal = $request->codigo_postal;
            $cliente->telefono = $request->telefono;
            $cliente->save();
              
         }else{
            Cliente::create($request->all());
         }
            
        //version larga, comentada
        // $study = new Study;
        // $study->code = $request->code;
        // $study->name = $request->name;
        // $study->abreviation = $request->abreviation;
        // $study->save();

        
        return redirect('/tienda');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
