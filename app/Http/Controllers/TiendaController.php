<?php

namespace App\Http\Controllers;

use App\Models\Tienda;
use Illuminate\Http\Request;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiendas = Tienda::all();
        return view('tienda.index', ['tiendas' => $tiendas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tienda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required',
            'tipo' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'descripcion' => 'required',
        ];
        $request->validate($rules);
        Tienda::create($request->all());
        return redirect('/tienda');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda = Tienda::find($id);

        return view('tienda.show', ['tienda' => $tienda]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tienda  $tienda
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        $tienda = Tienda::find($id);
        return view('tienda.edit', ['tienda' => $tienda]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombre' => 'required',
            'tipo' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'descripcion' => 'required'
            
        ];
       
        $request->validate($rules);

        //version larga, comentada
        // $study->code = $request->code;
        // $study->name = $request->name;
        // $study->abreviation = $request->abreviation;
        //version corta
        $tienda = Tienda::find($id);
        $tienda->fill($request->all());
        $tienda->save();
        return redirect('/tienda');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tienda = Tienda::find($id);
        $tienda->delete();
        return back();
    }

    public function Comprar(Request $request, $id)
    {
       
    }
}
