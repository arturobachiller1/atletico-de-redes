<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Models\Model' => 'App\Policies\ModelPolicy',
        'App\Models\Jugador' => 'App\Policies\JugadorPolicy',
        'App\Models\Tienda' => 'App\Policies\TiendaPolicy',
        'App\Models\Noticia' => 'App\Policies\NoticiaPolicy',
        'App\Models\Cliente' => 'App\Policies\ClientePolicy',
        'App\Models\Equipo' => 'App\Policies\EquipoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
