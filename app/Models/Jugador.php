<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'nombre', 'apellido', 'dorsal', 'posicion', 'nacionalidad', 'altura', 'peso', 'fechanacimiento', 'frase', 'imagen'];

    public function equipo(){
        return $this->belongsTo(Equipo::class);
    }
        
}
