<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['id','idproducto','precio','telefono', 'direccion', 'ciudad', 'codigo_postal'];
    
    use HasFactory;
}
