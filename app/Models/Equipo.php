<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use HasFactory;
     //$fillable indica que atributos se deben cargar en asignación masiva.
     protected $fillable = ['id', 'goles', 'partidosj', 'partidosg','partidose', 'partidosp', 'puesto', 'golesencontra', 'estadio'];

     public function Jugador()
     {
         return $this->hasMany(Jugador::class);
     }
}
