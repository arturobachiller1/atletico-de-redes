<?php

namespace Database\Seeders;

use App\Models\Noticia;
use Illuminate\Database\Seeder;

class NoticiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Noticia::create([
            'titulo' => 'Nace el atletico de redes',
            'imagen' => 'campo.jpg',
            'subtitulo' => 'Nace un nuevo equipo entre los peces pequeños',
            'descripcion'=> 'Tras una larga temporada nace el atletico de redes un club que jugara en la tercera regional grupo 1 categoria 14, el club esta fundado por Mabmerroi y cuyos jugadores estan ansiosos por labrarse un futuro en el ambito profesional del futbol.'
        ]); 
    }
}
