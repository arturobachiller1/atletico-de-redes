<?php

namespace Database\Seeders;

use App\Models\Tienda;
use Illuminate\Database\Seeder;

class TiendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tienda::create([
            'nombre' => 'Bufanda 3.0 con escudo',
            'tipo' => 'Bufanda',
            'imagen' => 'bufanda.png',
            'precio' => 13,
            'descripcion'=> 'una bufanda del equipo'
        ]); 
        Tienda::create([
            'nombre' => 'Camiseta 3.0 con escudo',
            'tipo' => 'Camiseta',
            'imagen' => 'camiseta.png',
            'precio' => 39,
            'descripcion'=> 'una camiseta del equipo'
            
        ]); 
        Tienda::create([
            'nombre' => 'Pantalon 3.0 con escudo',
            'tipo' => 'Pantalon',
            'imagen' => 'pantalon.png',
            'precio' => 25,
            'descripcion'=> 'un pantalon del equipo'
        ]); 
    }
}
