<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Equipo;

class EquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Equipo::create([
            'goles' => '0',
            'partidosj' => '0',
            'partidosg' => '0',
            'partidose' => '0',
            'partidosp' => '0',
            'puesto' => '17',
            'golesencontra' => '0',
            'estadio' => 'Caamouco stadium'
        ]);
// protected $fillable = ['id', 'goles', 'partidosj', 'partidosg', 
//'partidosp', 'puesto', 'golesencontra', 'estadio'];

    }
}
