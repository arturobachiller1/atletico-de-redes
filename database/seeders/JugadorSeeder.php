<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Jugador;

class JugadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*1*/
        Jugador::create([
            'nombre' => 'Arturo',
            'apellido' => 'Bachiller',
            'dorsal' => '1',
            'posicion' => 'Portero',
            'nacionalidad' => 'España',
            'altura' => '178',
            'peso' => '71',
            'fechanacimiento' => '2000-10-10',
            'frase' => 'vamo a juga',
            'imagen' => 'notfound.png'
        ]);     
        /*2*/
        Jugador::create([
            'nombre' => 'Raul',
            'apellido' => 'Cortes',
            'dorsal' => '17',
            'posicion' => 'Defensa',
            'nacionalidad' => 'España',
            'altura' => '183',
            'peso' => '79',
            'fechanacimiento' => '1999-11-13',
            'frase' => 'Dare lo mejor por este equipo',
            'imagen' => 'notfound.png'
        ]); 
        /*3*/
        Jugador::create([
            'nombre' => 'Sergio',
            'apellido' => 'Grobben',
            'dorsal' => '2',
            'posicion' => 'Defensa',
            'nacionalidad' => 'Belgica',
            'altura' => '181',
            'peso' => '83',
            'fechanacimiento' => '2000-02-29',
            'frase' => 'Me siento comodo aqui es un buen equipo',
            'imagen' => 'notfound.png'
        ]); 
        /*4*/
        Jugador::create([
            'nombre' => 'Lucas',
            'apellido' => 'Ainzua',
            'dorsal' => '3',
            'posicion' => 'Defensa',
            'nacionalidad' => 'Chile',
            'altura' => '186',
            'peso' => '90',
            'fechanacimiento' => '2000-11-13',
            'frase' => 'Siento que puedo darlo todo',
            'imagen' => 'notfound.png'
        ]); 
        /*5*/
        Jugador::create([
            'nombre' => 'Diego',
            'apellido' => 'Escriche',
            'dorsal' => '7',
            'posicion' => 'Defensa',
            'nacionalidad' => 'Jamaica',
            'altura' => '156',
            'peso' => '59',
            'fechanacimiento' => '2000-07-02',
            'frase' => 'Aupa el redes',
            'imagen' => 'notfound.png'
        ]); 
        /*6*/
        Jugador::create([
            'nombre' => 'Alejandro',
            'apellido' => 'Valenzuela',
            'dorsal' => '5',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'España',
            'altura' => '176',
            'peso' => '73',
            'fechanacimiento' => '2000-06-28',
            'frase' => 'Soy del betis desde chiquito',
            'imagen' => 'notfound.png'
        ]); 
        /*7*/
        Jugador::create([
            'nombre' => 'Alejandro',
            'apellido' => 'Ruiz',
            'dorsal' => '69',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'Peru',
            'altura' => '179',
            'peso' => '63',
            'fechanacimiento' => '2000-11-08',
            'frase' => 'Siempre nos ponemos objetivos altos',
            'imagen' => 'notfound.png'
        ]); 
        /*8*/
        Jugador::create([
            'nombre' => 'Radu',
            'apellido' => 'Matei',
            'dorsal' => '15',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'Rumania',
            'altura' => '177',
            'peso' => '68',
            'fechanacimiento' => '2000-5-25',
            'frase' => 'Estoy muy contento de pertenecer a este gran club',
            'imagen' => 'notfound.png'
        ]);   
        /*9*/
        Jugador::create([
            'nombre' => 'Persi',
            'apellido' => 'Gomez',
            'dorsal' => '35',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'España',
            'altura' => '173',
            'peso' => '57',
            'fechanacimiento' => '2000-08-17',
            'frase' => 'Estoy muy feliz, es un gran desafío para mí',
            'imagen' => 'notfound.png'
        ]); 
       

        /*10*/
        Jugador::create([
            'nombre' => 'Aritz',
            'apellido' => 'Ainzua',
            'dorsal' => '14',
            'posicion' => 'Delantero',
            'nacionalidad' => 'Chile',
            'altura' => '182',
            'peso' => '65',
            'fechanacimiento' => '2002-04-25',
            'frase' => 'Desde que llegué al club de pequeño quería estar donde estoy y pienso devolvérselo con ilusión, trabajo y mucho sacrificio',
            'imagen' => 'notfound.png'
        ]); 

        /*11*/
        Jugador::create([
            'nombre' => 'Esteban',
            'apellido' => 'Luzon',
            'dorsal' => '11',
            'posicion' => 'Delantero',
            'nacionalidad' => 'España',
            'altura' => '167',
            'peso' => '71',
            'fechanacimiento' => '2000-11-25',
            'frase' => 'Lo voy a dar todo por el equipo, por la afición y por el club',
            'imagen' => 'notfound.png'
        ]); 
        /*12*/
        Jugador::create([
            'nombre' => 'Jorge',
            'apellido' => 'Gamiz',
            'dorsal' => '13',
            'posicion' => 'Portero',
            'nacionalidad' => 'España',
            'altura' => '180',
            'peso' => '70',
            'fechanacimiento' => '2000-01-08',
            'frase' => 'Estoy muy feliz, muy contento e ilusionado ',
            'imagen' => 'notfound.png'
        ]); 

        /*13*/
        Jugador::create([
            'nombre' => 'Daniel',
            'apellido' => 'Rivas',
            'dorsal' => '22',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'Belgica',
            'altura' => '179',
            'peso' => '67',
            'fechanacimiento' => '2000-04-17',
            'frase' => 'Quiero marcar la historia. Vengo a un grandísimo club y vengo con muchísima ambición',
            'imagen' => 'notfound.png'
        ]); 
        /*14*/
        Jugador::create([
            'nombre' => 'Pablo',
            'apellido' => 'Rubio',
            'dorsal' => '28',
            'posicion' => 'Defensa',
            'nacionalidad' => 'España',
            'altura' => '173',
            'peso' => '75',
            'fechanacimiento' => '2000-02-10',
            'frase' => 'Desde niño soñaba con jugar en el Atlético',
            'imagen' => 'notfound.png'
        ]); 
        /*15*/
        Jugador::create([
            'nombre' => 'Apolo',
            'apellido' => 'Garcia',
            'dorsal' => '20',
            'posicion' => 'Delantero',
            'nacionalidad' => 'España',
            'altura' => '168',
            'peso' => '53',
            'fechanacimiento' => '2000-11-15',
            'frase' => 'Me gusta ir a por cada pelota como si fuera la última y dejar todo en la cancha',
            'imagen' => 'notfound.png'
        ]); 
        /*16*/
        Jugador::create([
            'nombre' => 'Daniel',
            'apellido' => 'Herrero',
            'dorsal' => '8',
            'posicion' => 'Mediocentro',
            'nacionalidad' => 'Rumania',
            'altura' => '168',
            'peso' => '80',
            'fechanacimiento' => '2000-12-24',
            'frase' => 'Es un reto bonito e importante',
            'imagen' => 'notfound.png'
        ]); 


    }
}
