<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EquipoController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\TiendaController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\ClienteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::get('prueba', function() {
    echo "hola mundo";
});

/*
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
    /*AÑADE AQUI LAS RUTAS QUE QUIERAS PROTEGER CON JWT*
});*/
Auth::routes();



Route::resource('equipos', EquipoController::class);
Route::resource('jugadores', JugadorController::class);
Route::resource('tienda', TiendaController::class);
Route::resource('noticias', NoticiaController::class);
Route::resource('clientes', ClienteController::class);

Route::get('/condiciones', function () {
    return view('condiciones');
});

Route::get('logout', function (){
    Auth::logout();
    return redirect('/equipos');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*                
                <p>
                    <label for="card-no">Numero de la tarjeta:</label>
                    <input type="text" name="card-no" value="" placeholder="Numero Tarjeta" wire:model="card-no">
                    @error('card-no') <span class="text-danger">{{$message}}</span> @enderror
                </p>
                <p>
                    <label for="exp-month">Fecha mes:</label>
                    <input type="text" name="exp-month" value="" placeholder="MM" wire:model="exp-month">
                    @error('exp-month') <span class="text-danger">{{$message}}</span> @enderror
                </p>
                <p>
                    <label for="exp-year">Fecha año:</label>
                    <input type="text" name="exp-year" value="" placeholder="YYYY" wire:model="exp-year">
                    @error('exp-year') <span class="text-danger">{{$message}}</span> @enderror
                </p>
                <p>
                    <label for="cvc">CVC:</label>
                    <input type="password" name="cvc" value="" placeholder="CVC" wire:model="cvc">
                    @error('cvc') <span class="text-danger">{{$message}}</span> @enderror
                </p>
*/
            